package com.java1234.shiro;

import com.java1234.common.ShiroUtil;
import org.apache.shiro.subject.Subject;
import org.junit.Test;

import java.util.Arrays;

public class RoleTest {

    @Test
    public void testHasRole() {
        Subject currentUser = ShiroUtil.login("classpath:shiro_role.ini", "java1234", "123456");

        System.out.println(currentUser.hasRole("role2") ? "有role1这个角色" : "没有role2这个角色");

        boolean[] results = currentUser.hasRoles(Arrays.asList("role1", "role2", "role3"));
        System.out.println(results[0] ? "有role1这个角色" : "没有role1这个角色");
        System.out.println(results[1] ? "有role2这个角色" : "没有role2这个角色");
        System.out.println(results[2] ? "有role3这个角色" : "没有role3这个角色");

        currentUser.checkRole("role2");

    }

}
