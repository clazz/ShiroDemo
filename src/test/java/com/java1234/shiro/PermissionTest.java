package com.java1234.shiro;

import com.java1234.common.ShiroUtil;
import org.apache.shiro.subject.Subject;
import org.junit.Test;

import java.util.Arrays;

public class PermissionTest {

    @Test
    public void testPermission() {
        Subject currentUser = ShiroUtil.login("classpath:shiro_role.ini", "java1234", "123456");
//        Subject currentUser = ShiroUtil.login("classpath:shiro_role.ini", "jack", "123");
        if (currentUser.isPermitted("user:select")) {
            System.out.println("有user:select权限");
        } else {
            System.out.println("没有user:select权限");
        }
    }

}
